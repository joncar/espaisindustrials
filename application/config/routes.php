<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['propiedad/(:any)-(:any)'] = "propiedad/index/$2";
$route['propiedad/lista'] = "propiedad/lista";
$route['propiedad/lista/(:any)'] = "propiedad/lista/listagrid";
$route['propiedad/listado'] = "propiedad/lista//listado";
$route['actualitat'] = "blog/frontend";
$route['actualitat/(:any)'] = "blog/frontend/read/$1";

$route['p/(:any)'] = "paginas/frontend/read/$1";
$route['p/(:any)/(:any)/(:any)'] = "paginas/frontend/read/$1/$2/$3";


$route['contacte'] = "main/contacto";
$route['404_override'] = 'main/error404';
$route['403_override'] = 'main/error403';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
