<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Panel.php');
class Registro extends Main {
        const IDROLUSER = 2;
        public function __construct()
        {
            parent::__construct();            
        }

        public function index($url = 'main',$page = 0)
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap2');
            $crud->set_table('user');
            $crud->set_subject('<span style="font-size:27.9px">Si eres nuevo entra por aquí</span>');            
            //Fields
            
            //unsets
            $crud->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_edit()
                 ->unset_list()
                 ->unset_export()
                 ->unset_print()
                 ->field_type('fecha_registro','invisible')
                 ->field_type('fecha_actualizacion','invisible')
                 ->field_type('status','invisible')
                 ->field_type('foto','invisible')
                 ->field_type('admin','invisible')
                 ->field_type('password','password')
                 ->field_type('cedula','invisible')
                 ->field_type('apellido_materno','invisible');
            
            $crud->display_as('password','Contraseña nuevo usuario')                 
                 ->display_as('email','Email de contacto')                 ;
            $crud->set_lang_string('form_add','');
            $crud->required_fields('password','email','nombre','apellido');
            //Displays             
            $crud->set_lang_string('form_save','Registrarse');

            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_after_insert(array($this,'ainsertion'));
            $crud->set_rules('email','Email','required|is_unique[user.email]');
            $output = $crud->render();
            $output->view = 'registro';
            $output->crud = 'user';
            $output->title = 'Registrarse';            
            $this->loadView($output);           
        }              
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function ainsertion($post,$id)
        {              
            //Asignar rol
            $this->db->insert('user_group',array('user'=>$id,'grupo'=>self::IDROLUSER));
            $this->enviarcorreo($this->db->get_where('user',array('id'=>$id))->row(),1);
            $this->user->login_short($id);
            return true;
        }
        
       
        function forget($key = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {
                if(empty($key)){
                if(empty($_SESSION['key'])){
                $this->form_validation->set_rules('email','Email','required|valid_email');
                if($this->form_validation->run())
                {
                    $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                    if($user->num_rows>0){
                        $_SESSION['key'] = md5(rand(0,2048));
                        $_SESSION['email'] = $this->input->post('email');
                        correo($this->input->post('email'),'reestablecimiento de contraseña',$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                        $_SESSION['msj'] = $this->success('Los pasos para la restauracion han sido enviados a su correo electronico');
                        header("Location:".base_url('registro/forget'));
                        //$this->loadView(array('view'=>'forget','msj'=>$this->success('Los pasos para la restauracion han sido enviados a su correo electronico')));
                    }
                    else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error('El correo que desea restablecer no esta registrado.')));
                }
                else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error($this->form_validation->error_string())));
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                    $this->form_validation->set_rules('key','Llave','required');
                    if($this->form_validation->run())
                    {
                        /*if($this->input->post('key') == $_SESSION['key'])
                        {*/
                            $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                            session_unset();
                            $this->loadView(array('view'=>'forget','msj'=>$this->success('Se ha restablecido su contraseña')));
                        /*}
                        else
                            $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));*/
                    }
                    else{
                        if(empty($_POST['key'])){
                        $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                        session_unset();
                        }
                        else
                        $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                    }
                }
                }
                else
                {
                    if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else{
                        $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                    }
                }
            }
        }        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
