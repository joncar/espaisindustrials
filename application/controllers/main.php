<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Main extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
            $this->load->helper('html');
            $this->load->helper('h');
            $this->load->database();
            $this->load->model('user');                    
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $this->load->library('traduccion');
            $this->load->model('querys');     
            $this->load->model('bdsource');
            $this->bdsource->init('ajustes',TRUE);
            $this->bdsource->init('categorias',FALSE);
            $this->bdsource->init('ajustes',TRUE);
            if(!isset($_SESSION['lang'])){
                $_SESSION['lang'] = 'ca';
            }
            $entrys = new Bdsource();
            $entrys->limit = array('3');
            $entrys->order_by = array('id','DESC');
            $entrys->where('idioma',$_SESSION['lang']);
            $entrys->init('blog',FALSE,'footerentrys');
            ini_set('show_errors',1);
            ini_set('display_errors',1);
            
            
        }

        public function index()
        {
            $this->get_entries();
            $propiedades = new Bdsource();
            $propiedades->limit = array('12','0');
            $propiedades->where('idioma',$_SESSION['lang']);
            $propiedades->order_by = array('rand()');
            @$propiedades->init('propiedades');            
            $recientes = new bdsource();            
            $recientes->order_by = array('id','DESC');
            $recientes->limit = array('12','0');
            $recientes->where('idioma',$_SESSION['lang']);
            $recientes->init('propiedades',FALSE,'recientes');            
            $this->bdsource->init('partners');
            $data = array(
                'view'=>'main',
                'propiedades'=>$this->propiedades,
                'recientes'=>$this->recientes,
                'blog'=>$this->blog,
                'partners'=>$this->partners
            );
            $this->loadView($data);                
        }              
        
        function get_entries(){
            $blog = new Bdsource();
            $blog->limit = array('4','0');
            $blog->where('idioma',$_SESSION['lang']);
            $blog->order_by = array('fecha','DESC');        
            $blog->init('blog');
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->link = site_url('actualitat/'.toURL($b->id.'-'.$b->titulo));
                $this->blog->row($n)->foto = base_url('application/modules/blog/images/fotos/'.$b->foto);
                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
                //$this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
            }
            if($this->blog->num_rows()>0){
                //$this->blog->tags = $this->blog->row()->tags;
            }
        }

        public function success($msj)
        {
                return '<div class="alert alert-success">'.$msj.'</div>';
        }

        public function error($msj)
        {
                return '<div class="alert alert-danger">'.$msj.'</div>';
        }

        public function login()
        {
                if(!$this->user->log)
                {	
                        if(!empty($_POST['email']) && !empty($_POST['pass']))
                        {
                                $this->db->where('email',$this->input->post('email'));
                                $r = $this->db->get('user');
                                if($this->user->login($this->input->post('email',TRUE),$this->input->post('pass',TRUE)))
                                {
                                        if($r->num_rows>0 && $r->row()->status==1)
                                        {
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('main').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
                                        }
                                        else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                                }
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                        }
                        else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url('registro/index/add'));
                }
                else header("Location:".base_url('registro/index/add'));
        }       

        public function unlog()
        {
                $this->user->unlog();                
                header("Location:".site_url());
        }

        function getHead($page){
            $ajustes = $this->db->get('ajustes')->row();
            $stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
            $page = str_replace('</head>',$stocookie.'</head>',$page);
            return $page;
        }

        function getBody($page){
            $ajustes = $this->db->get('ajustes')->row();        
            $stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
            $stocookie.= html_entity_decode($ajustes->cookies);
            $page = str_replace('</body>',$stocookie.'</body>',$page);
            $page= str_replace('</body>',$ajustes->analytics.'</body>',$page);
            return $page;
        }

        public function loadView($param = array('view'=>'main'))
        {
            if (is_string($param))
            $param = array('view' => $param);
            $ajustes = $this->db->get('ajustes')->row();
            if(is_object($param)){
                $param = (array)$param;
            }
            $param['title'] = $ajustes->titulo;
            $param['favicon'] = $ajustes->favicon;
            $param['keywords'] = $ajustes->keywords;
            $param['description'] = $ajustes->description;
            $page = $this->load->view('template', $param,true);
            $page = $this->getHead($page);
            $page = $this->getBody($page);
            echo $this->traduccion->traducir($page,$_SESSION['lang']);
            
        }
        
        public function traducir($idioma = 'ca'){
            $_SESSION['lang'] = $idioma;            
            header("Location:".$_SERVER['HTTP_REFERER']);
        }

        public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }                

         function error404(){
            $this->loadView(array('view'=>'errors/403'));
        }    
        
        function contacto(){
            if(empty($_POST)){
                $this->loadView(array('view'=>'contacto','title'=>'Contacte'));
            }else{
                $mensaje = '<h1>Hola, te han enviado un mensaje desde <a href="http://www.espaisindustrials.es">www.espaisindustrials.es</a></h1>';
                $mensaje.= '<h2>Datos</h2>';
                foreach($_POST as $n=>$p){
                    $mensaje.= '<p><b>'.ucfirst($n).'</b> = '.$p.'</p>';
                }
                correo($this->ajustes->correo,'Solicitud de contacto',$mensaje);
            }
        }
}
