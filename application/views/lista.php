<?php $this->load->view('includes/headerMain') ?>
<header id="banner">
    <div id="banner_container" class="container">
        <h3 class="banner-title">Llistat de Propietats</h3>
        <p class="banner-subtitle"></p>
    </div>
</header>
<div class="content-wrapper clearfix">
    <div id="title-listing" class="container">
        <div class="property-list-title">
            <?php if(!empty($_GET['categorias_id'])): ?>
                <?php 
                switch($_GET['categorias_id']){
                    case '3':echo 'Locals'; break;
                    case '2':echo 'Naus Industrials'; break;
                    case '1':echo 'Solars'; break;
                }
                ?>
            <?php else: ?>
            Llistat 
            <?php endif ?>
        </div>
        <div class="property-list-by">
            <a <?= empty($_GET['categorias_id'])?'class="current"':''?> href="javascript:changeCat(0)">Tots</a>            
            <a <?= !empty($_GET['categorias_id']) && $_GET['categorias_id']=='2'?'class="current"':''?> href="javascript:changeCat(2)">Naus Industrials</a>
            <a <?= !empty($_GET['categorias_id']) && $_GET['categorias_id']=='1'?'class="current"':''?> href="javascript:changeCat(1)">Solars</a>            
            <!--<a <?= !empty($_GET['categorias_id']) && $_GET['categorias_id']=='3'?'class="current"':''?> href="javascript:changeCat(3)">Locals</a>-->
        </div>
    </div><!-- /#title-listing -->
    <div class="container"><!-- container via hooks -->
        <div id="main" class="row-fluid">
            <section  id="content" class="span9" role="main">
                <div id="archive-wrapper">
                    <div class="property-sort">
                        <div class="sort-title">
                            <span class="sort-by">Filtrat per: </span>
                            <a <?= empty($_GET['tipo_venta'])?'class="current"':''?> href="javascript:changeTipo(0)">Tots</a>
                            <a <?= !empty($_GET['tipo_venta']) && $_GET['tipo_venta']=='2'?'class="current"':''?> href="javascript:changeTipo(2)">Lloguer</a>
                            <a <?= !empty($_GET['tipo_venta']) && $_GET['tipo_venta']=='1'?'class="current"':''?> href="javascript:changeTipo(1)">Venda</a>
                        </div>
                        <div class="grid-view hidden-phone">
                            <a href="javascript:changeview('')"><img src="<?= base_url().'/images/view-list.png' ?>" /></a>
                            <a href="javascript:changeview('listagrid')"><img src="<?= base_url().'/images/view-grid.png' ?>" /></a>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <?php $this->load->view('includes/fragmentos/'.$viewlist,array('propiedades'=>$propiedades)); ?>
                    </div>
                </div><!-- /#archive-wrapper -->
                <div class="property-pagination">
                    <ul class='page-numbers'>
                        <?php for($i=1;$i<=$total_pages;$i++): ?>
                        <li><a href='javascript:changePage("<?= $i ?>")'><span class='page-numbers <?= $i==$current_page?'current':'' ?>'><?= $i ?></span></a></li>
                        <?php endfor ?>
                    </ul>
                </div>				
            </section><!-- #content -->
            
            
            <section id="sidebar" class="span3" role="complementary">
                <aside id="property-search-widget-2" class="widget widget-property-search">
                    <h3 class="widget-title">Recerca de Propietats</h3>
                    <div class="content-widget">
                        <?= $this->load->view('includes/searchbox',array('formsubmit'=>$urlform)) ?>
                    </div><!-- /.content-widget -->
                </aside>
            </section><!-- #sidebar -->
            
            
            
            
        </div><!-- /#main -->
    </div><!-- /.container-->
</div><!-- /.content-wrapper -->
<?php $this->load->view('includes/footer') ?>
<script>
    function changeview(id){
        $("#formSearchBox").attr('action','<?= base_url('propiedad/lista') ?>/'+id);
        $("#formSearchBox").submit();
    }
</script>
