<?php $formsubmit = empty($formsubmit)?base_url('propiedad/lista/'):$formsubmit; ?>
<form class="advance-search-form form-inline clearfix" action="<?= $formsubmit ?>" method="get" id="formSearchBox">    
    <div class="option-bar type">
        <span class="selectwrap">
            <?php $sel = empty($_GET['categorias_id'])?"":$_GET['categorias_id']; ?>
            <?= form_dropdown('categorias_id',array(""=>"Immoble","1"=>"Solars","2"=>"Naus","5"=>'Naus logístiques'),$sel,'id="select-cat" class=""'); ?>            
        </span>
    </div>
    <div class="option-bar bedroom">
        <span class="selectwrap">
            <?php $sel = empty($_GET['metros'])?"":$_GET['metros']; ?>
            <?= form_dropdown('metros',array(
                ""=>"Superfície",
                "0-800"=>"0 - 800 m2",
                "800-1400"=>"800 - 1400 m2",
                "1400-2800"=>"1400 - 2800 m2",
                "2800-6800"=>"2800 - 6800 m2",
                "6800"=>"> 6800 m2"
                ),
                $sel,'id="select-metres" class=""'); ?>   
        </span>
    </div>
    <div class="option-bar status">
        <span class="selectwrap">
            <?php 
                $sel = empty($_GET['comarcas_id'])?0:$_GET['comarcas_id']; 
                $this->db->select('comarcas.*');
                $this->db->group_by('comarcas.id');
                $this->db->join('comarcas','comarcas.id = propiedades.comarcas_id');
                $this->db->order_by('comarca_nombre','ASC');
                $prop = $this->db->get_where('propiedades',array('idioma'=>$_SESSION['lang']));
            ?>

            <?= form_dropdown_from_query('comarcas_id',$prop,'id','comarca_nombre',$sel,'id="select-tipoventa"',FALSE,'','Zona'); ?>            
        </span>
    </div>        
    <input type="hidden" name="tipo_venta" id="select-tipoventa2" value="<?= empty($_GET['tipo_venta'])?'':$_GET['tipo_venta'] ?>">
    <input type="hidden" name="page" value="<?= empty($_GET['page'])?'1':$_GET['page']-1 ?>" id="pageSearchBox">
    <input type="hidden" name="order" value="<?= empty($_GET['order'])?'id_ASC':$_GET['order'] ?>" id="order">    
    <?php if($this->router->fetch_class()=='main'): ?>
        <div class="option-submit">
            <input type="submit" value="&nbsp;" class="advance-button-search">
        </div>
    <?php else: ?>
        <input type="submit" class="button button-search-widget" value="Buscar" />
    <?php endif ?>
</form>
<script>
    function changePage(val){
        $("#pageSearchBox").val(val);
        $("#formSearchBox").submit();
    }
    
    function changeTipo(id){
        $("#select-tipoventa2").val(id);
        $("#formSearchBox").submit();
    }
    
    function changeCat(id){
        $("#select-cat").val(id);
        $("#formSearchBox").submit();
    }
    
    function changeOrder(i){
        $("#order").val(i);
        $("#formSearchBox").submit();
    }
    
    function actualizarMetros(valor){
        jQuery("#select-metres").selectbox();
    }
    
    function actualizarPrecio(valor){
        var valores = [];
        switch(valor){
            case '2':
                valores.push({id:'0-1000',val:'0 a 1.000€'});
                valores.push({id:'1000-2500',val:'1.000 a 2.500€'});
                valores.push({id:'2500-5000',val:'2.500 a 5.000€'});                
                valores.push({id:'5000',val:'més 5.000€'});
            break;
            case '1':
                valores.push({id:'100000-250000',val:'100.000 a 250.000€'});
                valores.push({id:'250000-500000',val:'250.000 a 500.000€'});                
                valores.push({id:'500000-1000000',val:'500.000 a 1.000.000€'});
                valores.push({id:'2000000',val:'més 2.000.000€'});
            break;
        }
        var str = '<option value="">Preu</option>';
        for(var i in valores){
            str+= '<option value="'+valores[i].id+'">'+valores[i].val+'</option>';
        }
        jQuery("#select-precio").html(str);
        jQuery("#select-precio").parents('.selectwrap').find('.selectbox-wrapper').remove();
        jQuery("#select-precio").parents('.selectwrap').find('.selectbox').remove();
        jQuery("#select-precio").selectbox();
    }
   
</script>

<script>
         jQuery(document).ready(function(){
            jQuery("#select-cat").selectbox({
                onChangeCallback:function(param){
                    //actualizarMetros(param.selectedVal);
                }
            });
            actualizarMetros('<?= empty($_GET['categorias_id'])?0:$_GET['categorias_id'] ?>');
            
            jQuery("#select-tipoventa").selectbox({
                onChangeCallback:function(param){
                    //actualizarPrecio(param.selectedVal);
                }
            });
            actualizarPrecio('<?= empty($_GET['tipo_venta'])?0:$_GET['tipo_venta'] ?>');
        });
    </script>
