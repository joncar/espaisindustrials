<div class="row-fluid property-listing <?= $detail->tipo_venta==1?'status-35':'status-28' ?> clearfix">
    <div class="listing-image span5">
        <div class="property-image-container">
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>" title="<?= $detail->nombre_propiedad ?>">
                <img width="540" height="360" src="<?= base_url('uploads').'/'.$detail->foto_portada ?>" class="attachment-real-property-loop wp-post-image" alt="<?= $detail->nombre_propiedad ?>" title="<?= $detail->nombre_propiedad ?>" />
            </a>
        </div><!-- /.property-images-container -->
        <div class="listing-meta">
            <ul>
                <li class="meta-size" title="metres"><i class="ico-size"></i><?= number_format($detail->metros,0,',','.') ?>M<sup class="size-curr">2</sup></li>
                <?php if($detail->categorias_id==2): ?>
                        <?php if($detail->altura!==null): ?>
                            <li class="meta-bedroom" title="alçada"><i class="ico-bedroom"></i><?= $detail->altura ?>M</li>
                        <?php endif ?>
                        <?php if($detail->oficinas==1): ?>
                            <li class="meta-bedroom" title="Oficines"><i class="ico-bathroom"></i>SI</li>
                         <?php else: ?>
                            <li class="meta-bedroom" title="Oficines"><i class="ico-bathroom"></i>NO</li>
                        <?php endif ?>
                <?php else: ?>
                        <?php if(!empty($detail->edificacion_solar)): ?>
                            <li class="meta-bathroom" title="ocupació"><i class="ico-size-floor"></i><?= $detail->edificacion_solar ?></li>
                        <?php endif ?>
                        <?php if(!empty($detail->solar_finalista)): ?>
                            <li class="meta-bedroom" title="alçada"><i class="ico-bedroom"></i><?= $detail->solar_finalista ?></li>
                        <?php endif ?>
                <?php endif ?>
            </ul>
        </div>
    </div>
    <div class="listing-info span7">
        <div class="listing-title">
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>" title="<?= $detail->nombre_propiedad ?>"><?= $detail->nombre_propiedad ?></a>
        </div>
        <div class="listing-content">
            <div class="listing-property-price">
                <?php if($detail->precio!=0): ?>
                <?= number_format($detail->precio,0,',','.') ?><sup class="price-curr">€</sup>&nbsp;
                <span class="price-postfix"></span>
                <?php else: ?>
                    A Convenir 
                <?php endif ?>
            </div>
            <div class="listing-excerpt">
                <p><?= $detail->descripcion ?></p>
            </div>
        </div>
        <div class="listing-address">
            <i style="color: rgb(234, 77, 57); font-size: 30px;" class="fa fa-map-marker"></i>
            <?= $detail->direccion ?>
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>#property_map" role="button" target="_blank">Veure mapa</a>
        </div>
    </div>
    <div class="property-status <?= $detail->tipo_venta==1?'status-35-text':'status-28-text' ?>"><?= $detail->tipo_venta==1?'Venda':'Lloguer' ?></div>
</div><!-- /.property-listing -->
