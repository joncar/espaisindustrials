<div class="span3">
    <article class="property-item">
        <div class="property-images <?= $detail->tipo_venta==1?'status-35':'status-28' ?>">
            <a title="<?= $detail->nombre_propiedad ?>" href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
                <img width="540" height="360" title="<?= $detail->nombre_propiedad ?>" alt="<?= $detail->nombre_propiedad ?>" src="<?= base_url('uploads').'/'.$detail->foto_portada ?>">
            </a>
            <div class="<?= $detail->tipo_venta==1?'property-status status-35-text':'property-status status-28-text' ?>">
                    <?= $detail->tipo_venta==1?'Venda':'Lloguer' ?>
            </div>
        </div><!-- /.property-images -->
        <div class="property-attribute">
            <h3 class="attribute-title">
                <a title="<?= $detail->nombre_propiedad ?>" href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
                    <?= $detail->nombre_propiedad ?>
                </a>
            </h3>
            <span class="attribute-city">
                <?= $detail->direccion ?></span>
            <div class="attribute-price">
                <span class="attr-pricing">
                    <?php if($detail->precio!=0): ?><?= number_format($detail->precio,0,',','.') ?><sup class="price-curr">€</sup><?php else: ?>A Consultar<?php endif ?>
            </div>
        </div>
        <div class="property-meta clearfix">
            <div class="meta-size meta-block" title="metres"><i class="ico-size"></i><span class="meta-text"><?= $detail->metros ?>M<sup class="size-curr">2</sup></span></div>
            <?php if($detail->categorias_id==2): ?>
                    <?php if($detail->altura!==null): ?>
                        <div class="meta-bedroom meta-block" title="alçada"><i class="ico-bedroom"></i><span class="meta-text"><?= $detail->altura ?>M</span></div>
                    <?php endif ?>
                    <?php if($detail->oficinas==1): ?>
                            <div class="meta-bathroom meta-block" title="Oficines"><i class="ico-bathroom"></i><span class="meta-text">SI</span></div>
                     <?php else: ?>
                            <div class="meta-bathroom meta-block" title="Oficines"><i class="ico-bathroom"></i><span class="meta-text">NO</span></div>
                    <?php endif ?>
            <?php else: ?>
                    <?php if(!empty($detail->edificacion_solar)): ?>
                        <div class="meta-bathroom meta-block" title="ocupació"><i class="ico-size-floor"></i><span class="meta-text"><?= $detail->edificacion_solar ?></span></div>
                    <?php endif ?>
                    <?php if(!empty($detail->solar_finalista)): ?>
                        <div class="meta-bathroom meta-block" title="alçada"><i class="ico-bedroom"></i><span class="meta-text"><?= $detail->solar_finalista ?>M</span></div>
                    <?php endif ?>
            <?php endif ?>
        </div>
    </article>
</div>