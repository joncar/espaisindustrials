<div class="dsidx-media">
    <div class="dsidx-photo"><a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
            <img src="<?= base_url('uploads').'/'.$detail->foto_portada ?>" alt="<?= $detail->nombre_propiedad ?>" title="<?= $detail->nombre_propiedad ?>"></a>
    </div>
</div>
<div class="dsidx-primary-data">
    <div class="dsidx-address"><a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
        <?= $detail->nombre_propiedad ?></a>
    </div>
    <div class="dsidx-price">
        <?= is_numeric($detail->precio) && $detail->precio!=0?number_format($detail->precio,0,'.','.').'<sup class="price-curr">€</sup>':'A convenir'; ?>
    </div>
</div>
<div class="dsidx-secondary-data">
    <?php if($detail->categorias_id==2): ?>
        <?php if($detail->metros_planta!=null): ?>
            <div title="metres planta">Metres planta: <?= $detail->metros_planta ?>M<sup class="size-curr">2</sup></div>
        <?php endif ?>
        <?php if($detail->altura!==null): ?>
            <div  title="alçada">Alçada: <?= $detail->altura ?>M</div>
        <?php endif ?>
        <?php if($detail->oficinas==1): ?>
            <div  title="Oficines">Oficines: SI</div>                                
        <?php else: ?>
            <div  title="Oficines">Oficines: NO</div>
        <?php endif ?>
        <?php if($detail->instalacion_electiva==1): ?>
            <div  title="Instal.lació elèctrica">Instal.lació elèctrica</div>
        <?php endif ?>
        <?php if($detail->lavabos==1): ?>
            <div  title="Lavabos">Lavabos</div>
        <?php endif ?>
        <?php if($detail->patio==1): ?>
            <div  title="Pati">Pati</div>
        <?php endif ?>
    <?php else: ?>
            <?php if(!empty($detail->edificacion_solar)): ?>
                <div title="ocupació">Ocupació: <?= $detail->edificacion_solar ?></div>
            <?php endif ?>
            <?php if(!empty($detail->solar_finalista)): ?>
                <div title="alçada">Alçada: <?= $detail->solar_finalista ?></div>
            <?php endif ?>
    <?php endif ?>
            <div title="Dirección"><i style="color: rgb(234, 77, 57); font-size: 30px;" class="fa fa-map-marker"></i>
                <?= $detail->direccion ?>
                <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>#property_map" role="button" target="_blank">Veure mapa</a>
            </div>    
</div>