<div style="position:relative">
    <img class="map" src="images/maps/usa-map.png" usemap="#usa_map" alt="usa map" />
<!--<map name="usa_map" id="usa_map">   
    <?php $propiedad = $this->db->get_where('propiedades',array('comarcas_id'=>4,'idioma'=>$_SESSION['lang']))->num_rows(); ?>
    <?php if($propiedad>0): ?>
    <area id="bages" alt="" title="Bages - <?= $propiedad ?> propiedades" href="<?= site_url('propiedad/lista/') ?>?comarcas_id=4" shape="poly" coords="243,238,245,234,248,233,251,226,258,228,263,227,267,216,272,209,269,205,273,204,276,207,287,202,287,196,282,190,282,182,273,182,269,179,267,172,263,171,261,176,258,171,252,175,248,179,245,183,243,182,239,178,230,180,225,175,221,175,219,172,215,172,214,178,207,184,211,187,210,191,208,199,208,207,207,211,209,215,205,218,210,218,213,224,218,230,221,235,227,227,232,226,236,237" />    
    <?php endif ?>
    <?php $propiedad = $this->db->get_where('propiedades',array('comarcas_id'=>1,'idioma'=>$_SESSION['lang']))->num_rows(); ?>
    <?php if($propiedad>0): ?>
    <area id="anoia" alt="" title="Anoia - <?= $propiedad ?> propiedades" href="<?= site_url('propiedad/lista/') ?>?comarcas_id=1" shape="poly" coords="190,265,193,267,201,265,204,266,207,264,202,261,207,257,208,253,219,254,220,258,227,263,231,266,233,261,239,257,242,254,237,249,234,245,237,239,234,235,234,230,231,228,227,228,224,229,222,232,219,233,216,229,214,228,210,222,210,219,205,218,204,215,207,215,210,215,207,211,207,206,204,204,201,207,200,207,197,205,192,201,186,199,184,201,181,209,182,212,178,215,183,219,184,228,187,228,180,230,177,233,175,235,180,238,180,240,183,241,188,241,185,246,181,250,186,255,193,259" />
    <?php endif ?>
    <?php $propiedad = $this->db->get_where('propiedades',array('comarcas_id'=>2,'idioma'=>$_SESSION['lang']))->num_rows(); ?>
    <?php if($propiedad>0): ?>
    <area id="baix_llobregat" alt="" title="Baix Llobregat - <?= $propiedad ?> propiedades" href="<?= site_url('propiedad/lista/') ?>?comarcas_id=2" shape="poly" coords="237,238,243,239,247,241,250,244,254,251,254,258,261,264,264,265,270,267,272,272,277,274,276,281,277,290,282,291,278,295,258,300,255,298,250,293,244,294,240,290,245,285,247,283,245,277,246,269,243,259,241,255,241,252,237,248,233,245" />
    <?php endif ?>
    <?php $propiedad = $this->db->get_where('propiedades',array('comarcas_id'=>3,'idioma'=>$_SESSION['lang']))->num_rows(); ?>
    <?php if($propiedad>0): ?>
    <area id="alt_penedes" alt="" title="Alt Penedès - <?= $propiedad ?> propiedades" href="<?= site_url('propiedad/lista/') ?>?comarcas_id=3" shape="poly" coords="239,256,242,259,246,264,246,272,244,277,248,279,247,284,241,287,239,289,232,287,228,292,223,293,220,292,216,300,211,303,208,305,204,301,204,298,208,299,210,297,211,293,206,293,207,289,203,288,202,283,204,281,198,276,194,272,193,269,199,264,203,266,206,263,203,260,209,255,210,252,216,253,220,257,226,262,229,264" />    
    <?php endif ?>    
</map>
-->
    <?php foreach($this->db->get_where('comarcas')->result() as $c): ?>
        <?php $propiedad = $this->db->get_where('propiedades',array('comarcas_id'=>$c->id,'idioma'=>$_SESSION['lang']))->num_rows(); ?>
        <?php if($propiedad>0): ?>
            <a href='<?= site_url('propiedad/lista/') ?>?comarcas_id=<?= $c->id ?>' title="<?= $c->comarca_nombre ?> - <?= $propiedad ?> propiedades"><img src='<?= base_url('img/pin.png') ?>' style="position:absolute; top:<?= $c->posy ?>px; left:<?= $c->posx ?>px;"></a>
        <?php endif ?>        
    <?php endforeach ?>
</div>
