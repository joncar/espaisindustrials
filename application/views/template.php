<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en-US" class="no-js ie ie6 ie-lte7 ie-lte8 ie-lte9"><![endif]-->
<!--[if IE 7 ]><html lang="en-US" class="no-js ie ie7 ie-lte7 ie-lte8 ie-lte9"><![endif]-->
<!--[if IE 8 ]><html lang="en-US" class="no-js ie ie8 ie-lte8 ie-lte9"><![endif]-->
<!--[if IE 9 ]><html lang="en-US" class="no-js ie ie9 ie-lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="es-ES" class="no-js"><!--<![endif]-->
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= empty($title) ? 'Espais industrials' : $title ?></title>
        <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
        <meta name="description" content="<?= empty($keywords) ?'': $description ?>" />
        <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
        <?php 
        if(!empty($css_files) && !empty($js_files)):
        foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
        <?php endforeach; ?>
        <?php foreach($js_files as $file): ?>
        <script src="<?= $file ?>"></script>
        <?php endforeach; ?>                
        <?php endif; ?>
        <link rel='stylesheet' id='realexpert_bootstrap_main_css-css' href='<?= base_url('library/bootstrap/css/bootstrap.min.css') ?>' type='text/css' media='all' />
        <link rel='stylesheet' id='realexpert_bootstrap_responsive_css-css' href='<?= base_url('library/bootstrap/css/bootstrap-responsive.min.css') ?>' type='text/css' media='all' />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">                
        <link rel='stylesheet' href='<?= base_url('library/font-awesome/css/font-awesome-ie7.min.css') ?>' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= base_url('library/flexslider/flexslider.css') ?>' type='text/css' media='all' />
        <link media="all" type="text/css" href="http://cdn3.diverse-cdn.com/api/combo-css/config=dsidxpress.css/43e53d" id="dsidx-css" rel="stylesheet">
        <link rel='stylesheet' href='<?= base_url('style.css') ?>' type='text/css' media='all' />
        <script type='text/javascript' src='<?= base_url('js/jquery3e5a.js?ver=1.10.2') ?>'></script>
        <script type='text/javascript' src='<?= base_url('js/jquery-migrate.min1576.js?ver=1.2.1') ?>'></script>
        <style type="text/css">
                body{
                        font-family:ProximaNova;
                        color:#757c80;
                }
                input, button, select, textarea{
                        font-family:ProximaNova;
                }
                .status-28-text{
                        background-color : #ec4d3a;
                }
                .status-28{
                        border-bottom:5px solid #ec4d3a !important;
                }

                .status-35-text{
                        background-color : #007bb0;
                }
                .status-35{
                        border-bottom:5px solid #007bb0 !important;
                }
        </style>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=drawing,geometry,places"></script> 
        <script type='text/javascript' src='<?= base_url('js/jquery.selectbox.js') ?>'></script>   

        <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">     
        
</head>

    <body>
        <?php $this->load->view($view) ?>    
        
        <script type="text/javascript">
            
            jQuery(document).ready(function(){
                    
            // Tab contents = .inside
                    var tag_cloud_class = '#tagcloud';
                    //Fix for tag clouds - unexpected height before .hide()
                    var tag_cloud_height = jQuery( '#tagcloud').height();
                    jQuery( '.inside ul li:last-child').css( 'border-bottom','0px' ); // remove last border-bottom from list in tab content
                    jQuery( '.realTabs').each(function(){
                            jQuery(this).children( 'li').children( 'a:first').addClass( 'selected' ); // Add .selected class to first tab on load
                    });
                    jQuery( '.inside > *').hide();
                    jQuery( '.inside > *:first-child').show();
                    jQuery( '.realTabs li a').click(function(evt){ // Init Click funtion on Tabs
                            var clicked_tab_ref = jQuery(this).attr( 'href' ); // Strore Href value
                            jQuery(this).parent().parent().children( 'li').children( 'a').removeClass( 'selected' ); //Remove selected from all tabs
                            jQuery(this).addClass( 'selected' );
                            jQuery(this).parent().parent().parent().children( '.inside').children( '*').hide();
                            jQuery( '.inside ' + clicked_tab_ref).fadeIn(500);
                            evt.preventDefault();
                    });
            });
    </script>
    
    <script type='text/javascript' src='<?= base_url('js/jquery.form.min.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('js/jpages.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('library/bootstrap/js/bootstrap.min.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('js/jquery.jcarousel.min.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('js/jquery.flexslider.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('js/jquery.placeholder.js') ?>'></script>
    
    <script type='text/javascript' src='<?= base_url('js/map-highlight.js') ?>'></script>
    <script type='text/javascript'>
            /* <![CDATA[ */
            var slide = {"start":"1","interval":"5000"};
            /* ]]> */
            var input = document.getElementById('direccion'); 
            autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
    </script>
    
    <script type='text/javascript' src='<?= base_url('js/real-expert.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('js/selectivizr.min.js') ?>'></script>
    <script type='text/javascript' src='<?= base_url('js/respond.min.js') ?>'></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->    
    </body>
</html>

