<?php echo $output ?>
<script>
 var naves = '#metros_planta_field_box, #instalacion_electiva_field_box, #oficinas_field_box, #lavabos_field_box, #patio_field_box, #precio_alquiler_field_box';
 var terrenos = '#edificacion_solar_field_box, #solar_finalista_field_box, #solar_urbanizar_field_box';
 var logisticas = '#pb_field_box,#p1_field_box,#pati_field_box,#plano_field_box,#section_altura_field_box';
 $(naves).hide();
 $(terrenos).hide();
 $(logisticas).hide();
 
 $("#field-categorias_id").change(function(){
    switch($(this).val()){
        case '1': 
            //Terrenos
            $(naves).hide();
            $(logisticas).hide();
            $(terrenos).show();
        break;
        case '2':
            //Naves
            $(naves).show();
            $(logisticas).hide();
            $(terrenos).hide();
        break;
        case '5':
            //Naus logisticas
            $(logisticas).show();
            $(naves).hide();
            $(terrenos).hide();
        break;
    } 
 });
 
 $(document).ready(function(){
 if($("#field-categorias_id").val()!==''){
     $("#field-categorias_id").trigger('change');
 }
});
 
var slide = {"start":"1","interval":"5000"};
var input = document.getElementById('field-direccion'); 
var autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
autocomplete.addListener('place_changed', fillInAddress);
console.log(ubicacion.marker);
function fillInAddress(){
    var place = autocomplete.getPlace().geometry.location;    
    ubicacion.marker.setPosition(new google.maps.LatLng(place.lat(),place.lng()));
    ubicacion.map.panTo(new google.maps.LatLng(place.lat(),place.lng()));
    $("#field-ubicacion").val(ubicacion.marker.getPosition());    
}

function searchDireccion(direccion){   
        var geocoder = new google.maps.Geocoder();
        var address = direccion;
        geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    for(var i in results){                                    
                        CrearMarcas(results[i]);
                    }
                    if(scope.lugares!==undefined){
                        scope.lugares(results);
                    }
                } else {
                  alert('No se ha podido encontrar la ubicación indicada: ' + status);
                  window.history.back();
                }
        });
}

$(document).on('change','#field-direccion',function(){
    
});
</script>