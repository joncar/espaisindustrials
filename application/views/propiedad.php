<?php $this->load->view('includes/headerMain') ?>
<div class="container-full">					
    <header id="banner">
        <div id="banner_container" class="container">
            <h3 class="banner-title">Detall de la propietat</h3>
            <p class="banner-subtitle"></p>
        </div>
    </header>
    <div class="content-wrapper clearfix">
        <div class="container"><!-- container via hooks -->
            <div id="main" class="row-fluid">
                <section  id="content" class="span9" role="main">
                    <article class="status-publish hentry clearfix" id="property-56">
                        <div id="property_slider_wrapper">
                            <div id="myCarousel" class="carousel slide" style="max-height:450px;">
                                <!-- Carousel items -->
                                <div class="carousel-inner" style="max-height:390px;">
                                    <div class='item active' style="background:url(<?= base_url('uploads').'/'.$propiedad->foto_portada ?>); background-size:100% auto; background-repeat:no-repeat; background-position:center;">
                                        <img src="<?= base_url('uploads').'/'.$propiedad->foto_portada ?>" style="width:100%; visibility:hidden;">
                                    </div> 
                                    <?php foreach($fotos->result() as $f): ?>
                                        <div class='item' style="background:url(<?= base_url('uploads').'/'.$f->foto ?>); background-size:100% auto; background-repeat:no-repeat; background-position:center;">
                                            <img src="<?= base_url('uploads').'/'.$f->foto ?>" style="width:100%; visibility:hidden;">
                                        </div>
                                    <?php endforeach ?>
                                </div>
                                <div class="carousel-thumbnail">
                                    <!-- Carousel nav -->
                                    <a class="carousel-control left" href="#myCarousel" data-slide="prev" style="top:20px !important;">&lsaquo;</a>
                                    <a class="carousel-control right" href="#myCarousel" data-slide="next" style="top:20px !important;">&rsaquo;</a>
                                    <ol class="carousel-indicators hidden-phone" style="height:40px;">
                                        <li data-target='#myCarousel' data-slide-to='0' class='active' style="height:40px; background:url(<?= base_url('uploads').'/'.$propiedad->foto_portada ?>) no-repeat;background-size:cover;"></li>
                                        <?php foreach($fotos->result() as $n=>$f): ?>
                                            <li data-target='#myCarousel' data-slide-to='<?= $n+1 ?>' style="height:40px; background: url(<?= base_url('uploads').'/'.$f->foto ?>) no-repeat; background-size:cover;"></li>
                                        <?php endforeach ?>                                        
                                    </ol>
                                </div>
                            </div>
                        </div><!-- /#property_slider_wrapper -->
                        <div id="single_property_meta_wrapper">
                            <div class="single-property-meta clearfix <?= $propiedad->tipo_venta==1?'status-35-text':'status-28-text' ?>">
                                <span class="meta-size" title="metres"><i class="ico-size"></i><?= $propiedad->metros ?>M<sup class="size-curr">2</sup></span>                                
                                <?php if($propiedad->categorias_id==2): ?>
                                    <?php if($propiedad->metros_planta!=null): ?>
                                        <span class="meta-exterior" title="metres planta"><i class="ico-size-floor"></i><?= $propiedad->metros_planta ?>M<sup class="size-curr">2</sup></span>
                                    <?php endif ?>
                                    <?php if($propiedad->altura!==null): ?>
                                        <span class="meta-bedroom" title="alçada"><i class="ico-bedroom"></i><?= $propiedad->altura ?>M</span>
                                    <?php endif ?>
                                    <?php if($propiedad->oficinas==1): ?>
                                        <span class="meta-bathroom" title="Oficines"><i class="ico-bathroom"></i>SI</span>                                
                                    <?php else: ?>
                                        <span class="meta-bathroom" title="Oficines"><i class="ico-bathroom"></i>NO</span>
                                    <?php endif ?>
                                    <?php if($propiedad->instalacion_electiva==1): ?>
                                        <span class="meta-eletric" title="Instal.lació elèctrica"><i class="ico-electric"></i>SI</span>
                                    <?php endif ?>
                                    <?php if($propiedad->lavabos==1): ?>
                                        <span class="meta-bath" title="Lavabos"><i class="ico-bath"></i>SI</span>
                                    <?php endif ?>
                                    <?php if($propiedad->patio==1): ?>
                                        <span class="meta-exterior" title="Pati"><i class="ico-exterior"></i>SI</span>
                                    <?php endif ?>
                                <?php elseif($propiedad->categorias_id==5): ?>
                                    <?php if($propiedad->altura!=null): ?>
                                        <span class="meta-bedroom" title="Altura"><i class="ico-bedroom"></i><?= $propiedad->altura ?> m<sup>2</sup></span>
                                    <?php endif ?>
                                    <?php if($propiedad->pb!=null): ?>
                                        <span class="meta-exterior" title="Planta baixa"><i class="ico-pb"></i><?= !empty($propiedad->pb)?'SI':'NO' ?></span>
                                    <?php endif ?>
                                    <?php if($propiedad->p1!=null): ?>
                                        <span class="meta-exterior" title="Primera planta"><i class="ico-pb1"></i><?= !empty($propiedad->p1)?'SI':'NO' ?></span>
                                    <?php endif ?>
                                    <?php if($propiedad->pati!=null): ?>
                                        <span class="meta-exterior" title="Pati"><i class="ico-exterior"></i>SI</span>
                                    <?php endif ?>
                                    <?php if($propiedad->plano!=null): ?>
                                        <span class="meta-exterior" title="Planell de la propietat">
                                            <a href="<?= base_url('uploads/'.$propiedad->plano) ?>" style="color: #817d81;"><i class="ico-pdf" style="margin-left: 0"></i><span>Planell propietat</span></a>
                                        </span>
                                    <?php endif ?>
                                    <?php if($propiedad->section_altura!=null): ?>
                                        <span class="meta-exterior" title="Planell de secció alçada">
                                            <a href="<?= base_url('uploads/'.$propiedad->section_altura) ?>" style="color: #817d81;"><i class="ico-pdf" style="margin-left: 0"></i><span>Planell alçada</span></a>
                                        </span>
                                    <?php endif ?>
                                <?php else: ?>
                                    <?php if(!empty($propiedad->edificacion_solar)): ?>
                                        <span class="meta-eletric" title="ocupació"><i class="ico-size-floor"></i><?= $propiedad->edificacion_solar ?></span>
                                    <?php endif ?>
                                    <?php if(!empty($propiedad->solar_finalista)): ?>
                                        <span class="meta-bath" title="alçada"><i class="ico-bedroom"></i><?= $propiedad->solar_finalista ?>M</span>
                                    <?php endif ?>
                                <?php endif ?>
                                <!--<span class="meta-print visible-desktop"><i class="ico-print"></i>
                                    <span class="print-hidden"><a href="javascript:window.print()">Imprimir</a></span>
                                </span>-->
                                    <span class="meta-status" style="margin-right: 0; padding: 15px 13px;"><?= $propiedad->tipo_venta==1?'Venda':'Lloguer' ?></span>
                            </div>
                        </div>
                        <div class="single-property-content-wrapper">
                            <header class="single-property-header">
                                <h3 class="single-property-title"><?= $propiedad->nombre_propiedad ?></h3>
                                <p class="single-property-address"><?= $propiedad->direccion ?></p>
                            </header>
                            <div class="single-property-price">
                                <?php if(is_numeric($propiedad->precio)): ?>
                                <p><h3><?= number_format($propiedad->precio,0,'.','.') ?><sup class="price-curr">€</sup>&nbsp;<span class="price-postfix"></span></h3></p>
                                <?php else: ?>
                                <p><h3><?= $propiedad->precio ?>&nbsp;<span class="price-postfix"></span></h3></p>
                                <?php endif ?>
                            </div>
                            <div class="single-property-content">
                                <?= $propiedad->descripcion ?>
                            </div>
                            <div class="single-property-map">
                                <div id="the_map" class="map-wrap clearfix">
                                    <span class="map-label">Ubicació</span>
                                    <div id="property_map"></div>
                                </div>
                            </div>
                            <br/>
                            <?php if(!empty($propiedad->video)): ?>
                                <div class="single-property-map">
                                    <div class="map-wrap clearfix">
                                        <span class="map-label">Video</span>
                                        <div class="video-box"><?= $propiedad->video ?></div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <!-- Modal -->
                            <div id="contactAgent" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="contactAgentLabel" aria-hidden="true" style="height: auto; max-height:none;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3>
                                        Enviar un missatge al <?= $propiedad->agente ?>
                                    </h3>
                                </div>
                                <div class="modal-body" style="height: auto; max-height:none;">
                                    <form id="contact-agent-form" action="#" method="post">
                                        <div class="controls controls-row">
                                            <input class="span12" name="name" type="text" placeholder="*Propiedad " value='<?= 'Propietat '.$propiedad->nombre_propiedad ?>' required />
                                        </div>
                                        <div class="controls controls-row">
                                            <input class="span6" name="name" type="text" placeholder="*Nom " required />
                                            <input class="span6" name="email" type="email" placeholder="*email" required />
                                        </div>
                                        <div class="controls">
                                            <textarea name="message" class="span12" rows="10" placeholder="*Missatge" required></textarea>
                                        </div>
                                        <div class="controls">
                                            <input id="submit" class="btn btn-contact" type="submit" name="submit" value="Enviar" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </article>
                </section><!-- #content -->
                <section id="sidebar" class="span3" role="complementary">
                    <aside id="property_agent-2" class="widget property-agent">
                        <h3 class="widget-title">Agent</h3>
                        <div class="content-widget"><img src="<?= base_url('uploads/'.$propiedad->agente_foto) ?>" class="alignleft agent-widget" />
                            <div class="agent-widget-name"><?= $propiedad->agente ?></div>
                            <div class="agent-widget-phone"><i class="fa fa-phone"></i><?= $propiedad->agente_telefono ?></div>
                            <div class="agent-widget-email"><i class="fa fa-envelope"></i><?= $propiedad->agente_correo ?></div>
                            <div class="clear"></div>                            
                            <div class="agent-desc"><a role="button" data-toggle="modal" class="button button-search-widget" href="#contactAgent">Contactar</a></div></div><!-- /.content-widget -->
                    </aside>
                    <aside id="property-search-widget-2" class="widget widget-property-search">
                        <h3 class="widget-title">Recerca de Propietats</h3>
                        <div class="content-widget">
                            <?php $this->load->view('includes/searchbox'); ?>
                        </div><!-- /.content-widget -->
                    </aside>
                    <!-- 
<aside id="wolf-twitter-widget-2" class="widget wolf-twitter-widget">
                        <h3 class="widget-title">Ùltims Tweets</h3>
                        <div class="content-widget">
                            <?php $this->load->view('includes/fragmentos/widget-twitter'); ?>
                        </div><!~~ /.content-widget ~~>
                    </aside>    
 -->
                </section><!-- #sidebar -->
            </div><!-- /#main -->
            <div class="single-property-related ">
                <span class="map-label">Relacionats</span>
                <div class="row-fluid">
                    <?php foreach($relacionadas->result() as $p): ?>
                        <?php $this->load->view('includes/fragmentos/item',array('detail'=>$p)); ?>
                    <?php endforeach ?>
                    <?php if($relacionadas->num_rows==0): ?>
                        No existen propiedades relacionadas
                    <?php endif ?>
                </div>
            </div><!-- /.single-propety-related -->

        </div><!-- /.container-->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('includes/footer') ?>
</div><!-- .container-full -->
<script>
    <?php 
        $map = $propiedad->ubicacion;
        $map = str_replace('(','',$map);
        $map = str_replace(')','',$map);
        $map = explode(',',$map);
        echo 'var lat = "'.$map[0].'", lon = "'.$map[1].'"; ';
    ?>
    var mapOptions = {
        zoom: 9,
        center: new google.maps.LatLng(lat,lon)
    };   
    map = new google.maps.Map(document.getElementById('property_map'), mapOptions);   
    new google.maps.Marker({ position: new google.maps.LatLng(lat,lon), map: map, title: '<?= str_replace("'","\'",$propiedad->nombre_propiedad) ?>' });
</script>
