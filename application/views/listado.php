<div class="container-full">
    <?php $this->load->view('includes/headerMain') ?>
    <header id="banner">
        <div id="banner_container" class="container">
            <h3 class="banner-title">Llistat de Propietats</h3>
            <p class="banner-subtitle"></p>
        </div>
    </header>			
    <div class="content-wrapper clearfix">
        <div class="container"><!-- container via hooks -->						
            <div id="main" class="row-fluid">
                <section id="content" class="span9" role="main">
                    <div id="page-idx-container">
                        <article class="post-3005 ds-idx-listings-page type-ds-idx-listings-page status-publish clearfix page" id="post-3005">
                            <div id="" class="entry-content">                                
                                <div id="dsidx" class="dsidx-results">
                                    <div class="dsidx-paging-control">
                                        Propietats
                                        <?= $items+1 ?> - <?= $items+6 ?> de <?= $total_pages ?> |
                                        <a href="javascript:changePage('1')">
                                            Primer
                                        </a> |
                                        <a href="javascript:changePage('<?= $current_page-1 ?>')">
                                            Anterior »
                                        </a> |
                                        <a href="javascript:changePage('<?= $current_page+1 ?>')">
                                            Pròxim &gt;
                                        </a> |
                                        <a href="javascript:changePage('<?= $total_pages ?>')">
                                            Últim »
                                        </a>
                                    </div>
                                    <div class="dsidx-sorting-control">Ordenat per
                                        <?= form_dropdown('',array('id_DESC'=>'Temps al mercat, la més recent primer','id_ASC'=>'Temps al mercat, la més antiga primer','precio_DESC'=>'Preu, el més alt primer','precio_ASC'=>'Preu, el més baix primer'),$_GET['order'],'id="ordenar" style="height: 20px;margin-bottom: 0;margin-right: 0;margin-top: 0;padding: 0;width: 240px;"'); ?>
                                    </div>
                                    <div id="dsidx-map-control"><a href="javascript:ocultarmapa()">
                                            <img src="http://cdn1.diverse-cdn.com/api/images/dsidxpress/icons/map.png/193ad5"> Amaga el mapa</a>
                                    </div>
                                    <div id="dsidx-map" style="width:100%; height:300px; display:block;"></div>
                                    <ol id="dsidx-listings">
                                        <?php foreach($propiedades->result() as $p): ?>
                                        <li class="dsidx-listing">                                            
                                                <?php $this->load->view('includes/fragmentos/list2',array('detail'=>$p)); ?>                                           
                                        </li>
                                        <li><hr></li>
                                        <?php endforeach ?>
                                        <?php if($propiedades->num_rows==0): ?>
                                            <li>No hi ha propietats</li>
                                        <?php endif ?>
                                    </ol>

                                    <div class="dsidx-paging-control">
                                        Propietats
                                        <?= $items+1 ?> - <?= $items+6 ?> de <?= $total_pages ?> |
                                        <a href="javascript:changePage('1')">
                                            Primer
                                        </a> |
                                        <a href="javascript:changePage('<?= $current_page-1 ?>')">
                                            Anterior »
                                        </a> |
                                        <a href="javascript:changePage('<?= $current_page+1 ?>')">
                                            Pròxim &gt;
                                        </a> |
                                        <a href="javascript:changePage('<?= $total_pages ?>')">
                                            Últim »
                                        </a>
                                    </div>
                                </div>
                                <div class="realexpert-social-share">
                                    <span class="">Compartir a : </span>
                                    <a class="stwitter" href="https://twitter.com/intent/tweet?text=IDX Listing&amp;url=http://demo.puriwp.com/realexpert/idx/listings/idx-listing/&amp;via=" target="_blank">Twitter</a>
                                    <a class="sfacebook" href="https://www.facebook.com/sharer/sharer.php?u=http://demo.puriwp.com/realexpert/idx/listings/idx-listing/" target="_blank">Facebook</a>
                                    <a class="sgplus" href="https://plus.google.com/share?url=http://demo.puriwp.com/realexpert/idx/listings/idx-listing/" target="_blank">Google+</a>
                                </div>
                            </div><!-- .entry-content -->
                        </article>

                    </div>


                </section><!-- #content -->
                <section id="sidebar" class="span3" role="complementary">		 
                    <aside id="search-2" class="widget widget_search">
                        <h3 class="widget-title">Buscar</h3>
                        <div class="content-widget">
                            <?= $this->load->view('includes/searchbox',array('formsubmit'=>$urlform));  ?>
                        </div>
                    </aside>
                </section><!-- #sidebar -->
            </div><!-- /#main -->
        </div><!-- /.container via hooks-->
    </div><!-- /.content-wrapper -->
        <?php $this->load->view('includes/footer') ?>
</div><!-- .container-full -->
<script type="text/javascript">
    /* <![CDATA[ */
    var slide = {"start": "1", "interval": "5000"};
    /* ]]> */
    $(document).ready(function(){
        $(document).on('change','#ordenar',function(){
            changeOrder($(this).val());
        });
    });
    function ocultarmapa(){
        $("#dsidx-map").toggle(500);
    }
    
    <?php 
        if($propiedades->num_rows>0){
            $map = $propiedades->row()->ubicacion;
            $map = str_replace('(','',$map);
            $map = str_replace(')','',$map);
            $map = explode(',',$map);            
        }else{
            $map = array(0,0);
        }
        echo 'var lat = "'.$map[0].'", lon = "'.$map[1].'"; ';
    ?>
    var mapOptions = {
        zoom: 9,
        center: new google.maps.LatLng(lat,lon)
    };   
    map = new google.maps.Map(document.getElementById('dsidx-map'), mapOptions);   
    var markers = [];
    <?php foreach($propiedades->result() as $detail): ?><?php 
        $map = $detail->ubicacion;
        $map = str_replace('(','',$map);
        $map = str_replace(')','',$map);
        $map = explode(',',$map);                                                
    ?>
        var m = new marker();
        m.mark = new google.maps.Marker({ position: new google.maps.LatLng(<?= $map[0] ?>,<?= $map[1] ?>), map: map, title: '<?= str_replace("'","\'",$detail->nombre_propiedad) ?>' });
        m.url = "<?= site_url('propiedad/'.  toURL($detail->nombre_propiedad.'-'.$detail->id)) ?>";
        m.addEvent();
        markers.push(m);
    <?php endforeach ?>
    function marker(){
        this.mark = '';
        this.url = '';
        this.addEvent = function(){
            this.mark.parent = this;
            google.maps.event.addDomListener(this.mark,'mouseup',function(e){                
                document.location.href=this.parent.url;
            });
        }
    }
</script>
