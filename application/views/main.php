<div class="container-full">					
    <?php $this->load->view('includes/headerMain') ?>
    <div class="content-wrapper clearfix">
        <div id="headline_wrapper">
            <div id="headline_container" class="container">                
                <section class="headline-title">
                    <h3>T'oferim els millors espais industrials per a la teva activitat.</h3>
                    <p>
                        Solars i naus industrials per a qualsevol activitat, en règim de lloguer o venda. Ens adaptem a les teves necessitats
                    </p>
                </section>
                <section class="property-search">
                    <div class="row-fluid">
                        <div class="span5">
                            <?php $this->load->view('includes/fragmentos/areamaps') ?>
                        </div>
                        <div class="span7">
                            <div class="search-wrapper">
                                <div class="search-title">Buscar</div>
                                <div class="search-form-v1">
                                    <span class="search-or">o</span>
                                    <!--<p class="search-info">població, comarca, C.P. (separat amb comes)</p>-->
                                    <?= $this->load->view('includes/searchbox') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>	
        </div><!-- /#headline-wrapper -->

                    
        <div id="property_list" class="container">		
            <div id="title-listing" class="container">
                <div class="property-list-title">Actualitat</div>
            </div><!-- /#title-listing -->    
            <div class="row-fluid property-row elementos" style="margin-bottom: 50px;">
                <?php if($blog->num_rows()>0): ?>
                <div class="span4">
                    <a href='<?= $blog->row(0)->link ?>'>
                        <div class="headerblog" style="height:412px;">
                            <img src="<?= $blog->row(0)->foto ?>" alt='<?= $blog->row(0)->titulo ?>'>
                            <div class="contenido">
                                <div class="fecha"><?= strftime("%d %b %Y",strtotime($blog->row(0)->fecha)) ?></div>
                                <div class="titulo"><?= $blog->row(0)->titulo ?></div>
                            </div>                        
                        </div>
                    </a>
                </div>
                <?php endif ?>
                <?php if($blog->num_rows()>1 || $blog->num_rows()>2): ?>
                <div class="span4">
                    <div class="span12">
                        <a href='<?= $blog->row(1)->link ?>'>
                            <div class="headerblog" style="">
                                <img src="<?= $blog->row(1)->foto ?>" alt='<?= $blog->row(1)->titulo ?>'>
                                <div class="contenido">
                                    <div class="fecha"><?= strftime("%d %b %Y",strtotime($blog->row(1)->fecha)) ?></div>
                                    <div class="titulo"><?= $blog->row(1)->titulo ?></div>
                                </div>                            
                            </div>
                        </a>
                    </div>
                    <div id="span12lastchild" class="span12" >
                        <a href='<?= $blog->row(2)->link ?>'>
                            <div class="headerblog" >
                                <img src="<?= $blog->row(2)->foto ?>" alt='<?= $blog->row(2)->titulo ?>' >                            
                                <div class="contenido">
                                    <div class="fecha"><?= strftime("%d %b %Y",strtotime($blog->row(2)->fecha)) ?></div>
                                    <div class="titulo"><?= $blog->row(2)->titulo ?></div>
                                </div>                            
                            </div>
                        </a>
                    </div>
                </div>
                <?php endif ?>
                <?php if($blog->num_rows()>3): ?>
                <div class="span4">
                    <a href='<?= $blog->row(3)->link ?>'>
                        <div class="headerblog" style="height:412px;">
                            <img src="<?= $blog->row(3)->foto ?>" alt='<?= $blog->row(3)->titulo ?>'>                        
                            <div class="contenido">
                                <div class="fecha"><?= strftime("%d %b %Y",strtotime($blog->row(3)->fecha)) ?></div>
                                <div class="titulo"><?= $blog->row(3)->titulo ?></div>
                            </div>                        
                        </div>
                    </a>
                </div>
                <?php endif ?>
            </div>

        </div><!-- /#property_list -->
                    
                    
        <div id="property_list" class="container">		
            <div id="title-listing" class="container">
                <div class="property-list-title">Darreres Propietats</div>
                <div class="property-list-by">
                    
                    <a class="current" href="<?= site_url() ?>">Tots</a>
                    <?php foreach($this->categorias->result() as $c): ?>
                        <a class="" href="<?= site_url('propiedad/lista').'?categorias_id='.$c->id ?>"><?= $c->categorias_nombre ?></a>
                    <?php endforeach ?>
                </div>
            </div><!-- /#title-listing -->            
                <?php foreach($propiedades->result() as $n=>$p): ?>
                    <?php if($n==0 || $n==4 || $n==8): ?>
                        <div class="row-fluid property-row">
                    <?php endif ?>
                        <?php $this->load->view('includes/fragmentos/item',array('detail'=>$p)); ?>
                    <?php if($n==3 || $n==7 || $n==$propiedades->num_rows-1): ?>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>                
        </div><!-- /#property_list -->
        <div id="property_info">
            <div class="container carousel-wrapper">
                <div class="container" id="recent-title-listing">
                    <div class="recent-property-list-title">Propietats Recents</div>
                    <div class="recent-property-list-by">
                        <div class="jcarousel-control">
                            <a class="jcarousel-control-prev" href="#" data-jcarouselcontrol="true" style="background:white;color:#7C888E;padding:7px 8px; border-radius:2px">
                                    <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="jcarousel-control-next" href="#" data-jcarouselcontrol="true" style="background:white;color:#7C888E;padding:7px 8px; border-radius:2px">
                                    <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div><!-- /#title-listing -->
                <div class="jcarousel container" data-jcarousel="true" data-jcarouselautoscroll="true">
                    <div class="jcontainer" style="left: -1800px; top: 0px;">                        
                        <?php foreach($recientes->result() as $n=>$p): ?>
                                <?php $this->load->view('includes/fragmentos/recientesMain',array('detail'=>$p)); ?>                            
                        <?php endforeach ?>
                        <?php if($recientes->num_rows==0): ?>
                            No hi han propietats
                        <?php endif ?>
                    </div><!-- jcontainer -->
                </div><!-- /.jcarousel -->
            </div><!-- /.container -->
        </div>
        <div id="property_partner">
            <div class="container">
                <header class="partner-header">
                    <h3 class="partner-title">Zones</h3>
                </header>
                <p class="partner-excerpt">
                    Espais Industrials t'ajuda a trobar les naus i solars que busques en aquestes poblacions
                </p>
                <div id="partners_slider" class="partners-logo-wrapper">
                    <div class="partner-list">
                        <?php foreach($partners->result() as $p): ?>
                            <div class="partner-item">
                                
                                    <img width="170" height="55" src="<?= base_url('uploads/partners/'.$p->foto) ?>" class="attachment-partners-thumb wp-post-image" alt="graphicriver" title="graphicriver" />
                                
                            </div>
                        <?php endforeach ?>
                    </div>
                    <div class="partner-control">
                        <a href="#" class="partner-prev" style="color:black; background:#6A7982;color:white;padding:3px 8px; border-radius:2px"><i class="fa fa-chevron-left"></i></a>
                        <a href="#" class="partner-next" style="color:black; background:#6A7982;color:white;padding:3px 8px; border-radius:2px"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div><!-- /#property_partner -->	
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('includes/footer') ?>
</div><!-- .container-full -->
