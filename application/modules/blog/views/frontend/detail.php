<div class="container-full">
    <?php $this->load->view('includes/headerMain');  ?>
    <header id="banner">
        <div id="banner_container" class="container">
            <h3 class="banner-title">Actualitat</h3>
            <p class="banner-subtitle"></p>
        </div>
    </header>			
    <div class="content-wrapper clearfix">
        <div class="container"><!-- container via hooks -->
            <div id="main" class="row-fluid">
                <section id="content" class="span9" role="main">
                    <div id="post-content-container">
                        <a href="" title="<?= $detail->titulo ?>">
                            <img src="<?= base_url('application/modules/blog/images/fotos/'.$detail->foto) ?>" class="attachment-single-blog-post wp-post-image" alt="1" height="450" width="870"></a><article class="post-25 post type-post status-publish format-standard has-post-thumbnail hentry category-architecture category-decorations category-luxury category-sales tag-custom tag-interior clearfix">
                            <header>
                                <h2 class="entry-title">
                                    <a href="<?= site_url('actualitat/'.toURL($detail->id.'-'.$detail->titulo)) ?>" title="<?= $detail->titulo ?>" rel="bookmark">
                                        <?= $detail->titulo ?>
                                    </a>
                                </h2>
                                <div class="entry-meta">
                                    <span class="meta-parts">
                                        <time datetime="<?= date("Y-m-d",strtotime($detail->fecha))."T00:00:00+00:00"; ?>">
                                            <i class="fa fa-calendar"></i><?= date("d/m/Y",strtotime($detail->fecha)); ?>
                                        </time>
                                    </span>
                                    <span class="meta-parts">
                                        <i class="fa fa-comment-o"></i>
                                        <a href="<?= site_url('actualitat/'.toURL($detail->id.'-'.$detail->titulo)) ?>#comments"><?= $comentarios->num_rows ?> Comentaris</a>
                                    </span>
                                </div><!-- .entry-meta -->
                            </header>

                            <div class="entry-content clearfix">
                                <?= $detail->texto ?>
                            </div><!-- .entry-content -->

                        </article>

                        <section id="comments">
                            <h2 id="comments-title">
                                <?= $comentarios->num_rows ?> COMENTARIS
                            </h2>
                            <?php $this->load->view('frontend/_comentarios',array('comentarios'=>$comentarios)); ?>
                            <?php $this->load->view('frontend/_sendComentario',array('detail'=>$detail)); ?>
                        </section><!-- #comments -->	</div><!-- /#post-content -->
                </section><!-- #content -->
                <section id="sidebar" class="span3" role="complementary">

                    <aside class="widget widget_search" id="search-2">
                        <h3 class="widget-title">Buscar</h3>
                        <div class="content-widget">
                            <form action="<?= base_url('actualitat') ?>" method="get" role="search" class="form-search" id="searchform">
                                <input type="text" placeholder="Buscar aquí" class="span10" id="s" name="direccion">
                                <button title="Buscar!" type="submit" class="button button-search-widget button-large">Buscar</button>
                            </form><!-- #searchform -->
                        </div><!-- /.content-widget -->
                    </aside>
                    <!-- 
<aside id="wolf-twitter-widget-2" class="widget wolf-twitter-widget">
                        <h3 class="widget-title">Ùltims Tweets</h3>
                        <div class="content-widget">
                            <?php $this->load->view('includes/fragmentos/widget-twitter'); ?>
                        </div><!~~ /.content-widget ~~>
                    </aside>     
 -->
                </section><!-- #sidebar -->
            </div><!-- /#main -->
        </div><!-- /.container via hooks-->		</div><!-- /.content-wrapper -->
        <?php $this->load->view('includes/footer'); ?>
</div><!-- .container-full -->