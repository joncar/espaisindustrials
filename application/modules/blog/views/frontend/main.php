<div class="container-full">
    <?php $this->load->view('includes/headerMain') ?>

    <header id="banner">
        <div id="banner_container" class="container">
            <h3 class="banner-title">Actualitat</h3>
            <p class="banner-subtitle"></p>
        </div>
    </header>			
    <div class="content-wrapper clearfix">
        <div class="container">
            <div id="main" class="row-fluid">
                <section id="content" class="span9" role="main">
                    <div id="post-content-container">
                        <?php foreach($detail->result() as $d): ?>
                            <?php $this->load->view('frontend/_entry',array('detail'=>$d)); ?>
                        <?php endforeach ?>
                        <?php if($detail->num_rows==0): ?>
                            No hi ha entrades
                        <?php endif ?>
                    </div><!-- /.post-content-container -->
                    <div class="blog-pagination">
                        <ul class="page-numbers">
                            <?php for($i=1;$i<=$total_pages;$i++): ?>
                                <li><a href='javascript:changePage("<?= $i ?>")'><span class='page-numbers <?= $i==$current_page?'current':'' ?>'><?= $i ?></span></a></li>
                            <?php endfor ?>                            
                        </ul>
                    </div>

                </section><!-- #content -->                
                <section id="sidebar" class="span3" role="complementary">                    
                    <aside class="widget widget_search" id="search-2" style="margin-bottom:10px;">
                        <h3 class="widget-title">Buscar</h3>
                        <div class="content-widget" style="height:90px;">
                            <form action="<?= base_url('actualitat') ?>" method="get" role="search" class="form-search" id="searchform">
                                <input type="text" placeholder="Buscar aquí" class="span10" id="s" name="direccion">
                                <input type="hidden" name="page" id="page" value="<?= empty($_GET['page'])?1:$_GET['page'] ?>">
                                <button title="Buscar!" type="submit" class="button button-search-widget button-large">Buscar</button>
                            </form><!-- #searchform -->
                        </div><!-- /.content-widget -->
                    </aside>
                    <!-- 
<aside id="wolf-twitter-widget-2" class="widget widget_search">
                        <h3 class="widget-title">Últims Tweets</h3>
                        <div class="content-widget">
                            <?php $this->load->view('includes/fragmentos/widget-twitter'); ?>
                        </div><!~~ /.content-widget ~~>
                    </aside>
 -->
                </section><!-- #sidebar -->
            </div><!-- /#main -->
        </div><!-- /.container via hooks-->
    </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('includes/footer') ?>
</div><!-- .container-full -->
<script>
    function changePage(id) {
        $("#page").val(id);
        $("#searchform").submit();
    }

    function changeCategoria(id) {
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>