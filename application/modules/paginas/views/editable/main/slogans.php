<div class="container-fluid">
    <div class="products--style-2">
        <div class="products__inner">
            <div class="row row-no-gutter">
                <div class="col-xs-12 col-md-6 col-lg-4">
                    <div class="product__item">
                        <figure>
                            <img src="<?= base_url() ?>img/blank.gif" style="background-image: url('<?= base_url() ?>img/gall_img/2_col/6.jpg');" alt="demo" />
                        </figure>

                        <div class="product__item__description">
                            <div class="product__item__description__inner">
                                <h2 class="product__item__title">sweet<br />Grape</h2>

                                Works discount secure
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-4">
                    <div class="product__item">
                        <figure>
                            <img src="<?= base_url() ?>img/blank.gif" style="background-image: url('<?= base_url() ?>img/gall_img/4_col/15.jpg');" alt="demo">
                        </figure>

                        <div class="product__item__description">
                            <div class="product__item__description__inner">
                                <h2 class="product__item__title">Juicy<br />cherry</h2>

                                First superior full-bodied drink
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6 col-lg-4">
                    <div class="product__item">
                        <figure>
                            <img src="<?= base_url() ?>img/blank.gif" style="background-image: url('<?= base_url() ?>img/gall_img/4_col/10.jpg');" alt="demo">
                        </figure>

                        <div class="product__item__description">
                            <div class="product__item__description__inner">
                                <h2 class="product__item__title">Beautiful<br />Strawberry</h2>

                                Odor to yummy high racy bonus soaking
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>