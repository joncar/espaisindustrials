<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Añadir paginas</h1>
    </div>
    <div class="panel-body">
        <?php if(!empty($_GET['msj']))echo '<div class="alert alert-danger">'.$_GET['msj'].'</div>'; ?>
        <form method="post" action="<?= base_url('paginas/admin/paginas/insert') ?>">
            <div class="form-group">
              <label for="text">Nombre del archivo* </label>
              <input type="text" name="nombre" class="form-control" id="text" placeholder="Nombre de archivo">
            </div>       
            <div class="form-group">    
                <label for="text">Template</label>
                <select name="template" name="template" class="form-control">
                    <option value="">Ninguno</option>
                    <?php 
                        $pages = scandir('theme');
                        foreach($pages as $p): 
                    ?>
                        <?php if(strpos($p,'.html')): ?>
                            <option value="<?= $p ?>"><?= $p ?></option>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>      
            <button type="submit" class="btn btn-default">Añadir y editar</button>
        </form>
    </div>
</div>