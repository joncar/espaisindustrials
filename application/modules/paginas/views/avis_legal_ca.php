<div class="container-full">					
    <?php $this->load->view('includes/headerMain') ?>
    <div class="content-wrapper clearfix">
        
        <div id="property_list" class="container">		
            <div id="title-listing" class="container">
                <div class="property-list-title">Avis Legal</div>
            </div><!-- /#title-listing -->    
            <div class="row-fluid property-row elementos" style="margin-bottom: 50px;">
                <?= $this->db->get('ajustes')->row()->aviso_legal_ca ?>
            </div>
        </div><!-- /#property_list -->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('includes/footer') ?>
</div><!-- .container-full -->
