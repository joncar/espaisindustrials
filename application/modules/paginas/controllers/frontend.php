<?php 
require_once APPPATH.'controllers/panel.php';    
class Frontend extends Main{        
    function __construct() {
        parent::__construct();
        $this->load->model('querys');
        $this->load->library('form_validation');
    }        
    
    function read($url){        
        $params = $this->uri->segments;
        $this->load->model('querys');            
        $this->loadView(
            array(
                'view'=>'read',
                'theme'=>$url,
                'page'=>$this->load->view($url,array(),TRUE),
                'title'=>ucfirst(str_replace('-',' ',$url))                    
            )
        );
    }
    
    function getFormReg($x = '2'){                    
        return $this->querys->getFormReg($x);
    }
    
    function editor($url){            
        $this->load->helper('string');
        if(!empty($_SESSION['user']) && $this->user->admin==1){                
            $page = $this->load->view($url,array(),TRUE);
            $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
        }else{
            redirect(base_url());
        }
    }
    
    function contacto(){
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('name','Nombre','required');
        $this->form_validation->set_rules('message','Comentario','required');
        if($this->form_validation->run()){
            $this->enviarcorreo((object)$_POST,1,'info@byebyegroup.com');                
            $_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
        }else{                
           $_SESSION['msj'] = $this->success('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');               
        }             
        if(!empty($_GET['redirect'])){
            redirect($_GET['redirect']);
        }else{
            redirect(base_url('p/contacto').'#contactenosform');
        }
    }
    
    function enviarCurriculum(){
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('nombre','Nombre','required');
        $this->form_validation->set_rules('mensaje','Comentario','required');
        //$this->form_validation->set_rules('curriculum','Curriculum','required');            
        if($this->form_validation->run() && !empty($_FILES['curriculum'])){
            get_instance()->load->library('mailer');
            get_instance()->mailer->mail->AddAttachment($_FILES['curriculum']['tmp_name'],$_FILES['curriculum']['name']);
            $this->enviarcorreo((object)$_POST,2,'info@finques-sasi.com');
            //$_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
            echo json_encode(array('success'=>TRUE,'message'=>'Gracias por contactarnos, en breve le contactaremos'));
        }else{
            //$_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
           echo json_encode(array('success'=>FALSE,'message'=>'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>'));
        }
        /*if(!empty($_GET['redirect'])){
            redirect($_GET['redirect']);
        }else{
            redirect(base_url('p/contacto'));
        }*/
    }
    
    function subscribir(){
        $this->form_validation->set_rules('email','Email','required|valid_email');
        if($this->form_validation->run()){
            $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
            $success = $emails->num_rows()==0?TRUE:FALSE;
            if($success){
                $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                echo $this->success('Subscrito satisfactoriamente');
            }else{
                echo $this->error('Correo ya existente');
            }
        }else{
            echo $this->error($this->form_validation->error_string());
        }
    }
    
    function unsubscribe(){
        if(empty($_POST)){
            $this->loadView('includes/template/unsubscribe');
        }else{
            $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
            $success = $emails->num_rows()>0?TRUE:FALSE;
            if($success){
                $this->db->delete('subscritos',array('email'=>$_POST['email']));
                echo $this->success('Correo desafiliado al sistema de noticias');
            }            
            $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
        }
    }
    
    function pdf(){
        require_once APPPATH.'libraries/html2pdf/html2pdf.php';
        $papel = 'L';
        $orientacion = 'P';
        $html2pdf = new HTML2PDF();
        $html2pdf->setDefaultFont('raleway');
        $html2pdf->addFont("ralewayb","B");
        $html2pdf->addFont("titanone","");
        $menu = $this->db->get('menu_del_dia')->row();
        $html = $menu->pdf;
        foreach($menu as $n=>$v){
            $html = str_replace('['.$n.']', str_replace('&bull;','<br/> &bull;',strip_tags($v)),$html);
        }
        $html = str_replace('[precio]',$this->db->get('ajustes')->row()->precio_menu_dia.'€',$html);
        $html = $this->load->view('pdf',array('html'=>$html),TRUE);
        $html2pdf->writeHTML($html); 
        //echo $this->db->get('menu_del_dia')->row()->pdf;
        ob_end_clean();         
        $html2pdf->Output('Menu-del-dia-'.date("dmY").'.pdf');
    }

    function galeria(){
        $galeria = $this->db->get_where('categoria_galeria');
        foreach($galeria->result() as $n=>$g){
            $this->db->order_by('orden','ASC');
            $galeria->row($n)->fotos = $this->db->get_where('galeria',array('categoria_galeria_id'=>$g->id));
        }
        $page = $this->load->view('_galeria',array('galeria'=>$galeria),TRUE);

        $this->loadView(
            array(
                'view'=>'read',
                'theme'=>'galeria',
                'page'=>$page,
                'title'=>'Galeria'                   
            )
        );
    }

    function videos(){
        $galeria = $this->db->get_where('categoria_videos');
        foreach($galeria->result() as $n=>$g){
            $this->db->order_by('orden','ASC');
            $galeria->row($n)->videos = $this->db->get_where('videos',array('categoria_videos_id'=>$g->id));
        }
        $page = $this->load->view('_videos',array('videos'=>$galeria),TRUE);

        $this->loadView(
            array(
                'view'=>'read',
                'theme'=>'videos',
                'page'=>$page,
                'title'=>'Videos'                   
            )
        );
    }

    function buscar(){
        if(!empty($_GET['q'])){
            $this->db->or_like('texto1',$_GET['q']);
            $this->db->or_like('texto2',$_GET['q']);
        }
        $blog = $this->db->get_where('buscador');
        $page = $this->load->view('buscador',array('resultados'=>$blog,'q'=>$_GET['q']),TRUE);        
        $page = str_replace('[header]',$this->load->view('includes/template/header',array(),TRUE),$page);
        $this->loadView(array('view'=>'read','page'=>$page));
    }

    function search(){
        if(!empty($_GET['q'])){
            if(empty($_SESSION[$_GET['q']])){
                $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3Amovilessat.es+'.urlencode($_GET['q']).'&oq=site%3Adiscotecarecords.com+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
            }
            $result = $_SESSION[$_GET['q']];
            preg_match_all('@<div id=\"ires\">(.*)</div>@si',$result,$result);
            $resultado = $result[0][0];
            $resultado = fragmentar($resultado,'<ol>','</ol>');
            $resultado = $resultado[0];

       
            $resultado = str_replace('https://www.movilessat.es/url?q=','',$resultado);
            $resultado = str_replace('/url?q=','',$resultado);
            $resultado = explode('<div class="g">',$resultado);                
            foreach($resultado as $n=>$r){
                if(strpos($r,'/search?q=site:')){
                    unset($resultado[$n]);
                    continue;
                }
                $resultado[$n] = '<div class="g">'.$r;                    
                $resultado[$n] = substr($r,0,strpos($r,'&'));
                $pos = strpos($r,'">')+2;
                $rr = substr($r,$pos);
                $pos = strpos($rr,'">');
                $rr = substr($rr,$pos);                    
                $resultado[$n].= $rr;
                $resultado[$n] = '<div class="g">'.$resultado[$n];
                $resultado[$n] = utf8_encode($resultado[$n]);
            }
            
            $this->loadView(array(
                'view'=>'read',
                'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                'result'=>$resultado,
                'title'=>'Resultado de busqueda'
            ));

        }
    }
}
