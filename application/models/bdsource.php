<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Bdsource extends CI_Model{
    //Variables de 6x1 para el calculo de el 6x1
    protected $tablename;
    protected $id = '';
    public $primary_key = 'id';
    public $join = array();
    public $where = array();
    public $or_where = array();
    public $like = array();
    public $or_like = array();
    public $result = array();
    public $order_by = '';
    public $limit = '';
    public $alias = '';
    public $select = '';
    public $filters = '';
    public $filter_order = 'order_by';
    
    function __construct($tablename = '',$autoinit = FALSE)
    {
        parent::__construct();
        if(!empty($tablename)){
            $this->tablename = $tablename;
        }
        if($autoinit){
            $this->init();
        }
    }
    
    public function init($tablename = '',$group = FALSE,$alias = '',$createglobalwithresult = TRUE){
        $this->alias = $alias;
        if(empty($tablename) && empty($this->tablename)){
            $this->result = null;
        }else{
            if(!empty($tablename)){
                $this->tablename = $tablename;
            }
        }
        $this->refresh($group,$createglobalwithresult);        
    }
    
    protected function apply_filters(){
        if(!empty($_GET)){
            $data = $_GET;
        }
        else if(!empty($_POST)){
            $data = $_POST;
        }
            
        foreach($this->filters as $f){            
            if(isset($data[$f]) && !empty($data[$f])){
                $this->db->where($f,$data[$f]);
            }
        }
        
        if(!empty($data[$this->filter_order])){
            $l = explode('_',$data[$this->filter_order]);
            if(!empty($l[1])){
                $this->db->order_by($l[0],$l[1]);
            }
        }
    }
    
    protected function refresh($group,$createglobalwithresult){
        if(!empty($this->tablename)){
            $alias = empty($this->alias)?$this->tablename:$this->alias;
            
            foreach($this->join as $j){            
                $this->db->join($j,$j.'.id = '.$this->tablename.'.'.$j.'_id');
            }

            foreach($this->where as $n=>$j){             
                if($j==NULL || $j==null){
                    $this->db->where($n,NULL,FALSE);
                }else{
                    $this->db->where($n,$j);
                }
            }

            foreach($this->or_where as $n=>$j){
                if($j==NULL || $j==null){
                    $this->db->or_where($n,NULL,FALSE);
                }else{
                    $this->db->or_where($n,$j);
                }
            }

            foreach($this->like as $n=>$j){             
                if($j==NULL || $j==null){
                    $this->db->like($n,NULL,FALSE);
                }else{
                    $this->db->like($n,$j);
                }
            }

            foreach($this->or_like as $n=>$j){             
                if($j==NULL || $j==null){
                    $this->db->or_like($n,NULL,FALSE);
                }else{
                    $this->db->or_like($n,$j);
                }
            }

            if(!empty($this->order_by)){
                $this->db->order_by($this->order_by[0],$this->order_by[1]);
            }

            if(!empty($this->limit)){                
                $this->limit[1] = !isset($this->limit[1])?0:$this->limit[1];
                $this->db->limit($this->limit[0],$this->limit[1]);
            }

            if(!empty($this->select)){
                $this->db->select($this->select,FALSE);
            }
            
            //Filtros
            if(!empty($this->filters)){
                $this->apply_filters();
            }

            $this->result = $this->db->get($this->tablename);

            if($group && $this->result->num_rows()==1){
                $result = $this->result->row();
            }
            elseif($group && $this->result->num_rows()==0){
                $result = null;
            }else{
                $result = $this->result;
            }
            if($createglobalwithresult){
                get_instance()->{$alias} = $result;        
            }
            $this->num_rows = $this->result->num_rows();
            $this->row(0);
        }else{
            throw new Exception('Nombre de tabla no declarada',503);
        }
    }
    
    public function where($field,$value = NULL){
        if(empty($this->where)){
            $this->where = array();
        }
        if(!is_array($field)){            
            $this->where[$field] = $value;
        }else{
            $this->db->where($field);
        }
    }
    
    public function or_where($field,$value){
        if(empty($this->or_where)){
            $this->where = array();
        }
        $this->or_where[$field] = $value;
    }
    
    public function like($field,$value){
        if(empty($this->like)){
            $this->like = array();
        }
        $this->like[$field] = $value;
    }
    
    public function or_like($field,$value){
        if(empty($this->or_like)){
            $this->or_like = array();
        }
        $this->or_like[$field] = $value;
    }
    
    public function innerjoin($table,$relationship = ''){
        if(empty($relationship)){
            $relationship = $this->tablename;
        }
        $this->db->join($table,$table.'.id = '.$relationship.'.'.$table.'_id','inner');
    }
    public function leftjoin($table,$relationship = ''){
        if(empty($relationship)){
            $relationship = $this->tablename;
        }
        $this->db->join($table,$table.'.id = '.$relationship.'.'.$table.'_id','left');
    }
    public function rightjoin($table,$relationship = ''){
        if(empty($relationship)){
            $relationship = $this->tablename;
        }
        $this->db->join($table,$table.'.id = '.$relationship.'.'.$table.'_id','right');
    }
    
    public function joinNormal($table,$condition = '',$type = null){
        if(is_array($table)){
            foreach($table as $t){
                $type = isset($t[2])?$t[2]:null;
                $this->db->join($t[0],$t[1],$type);
            }
        }else{
            $this->db->join($table,$condition,$type);
        }
    }
    
    public function group_by($grup){
        $this->db->group_by($grup);
    }
    
    public function row($id = 0,$return = FALSE){
        if(!empty($this->result) && $this->result->num_rows()>0){            
            foreach($this->result->row($id) as $n=>$r){
                if(($n=='id' && empty($this->id)) || $n!='id'){
                    $this->$n = $r;
                }
            }
            if($return){
                return $this->result->row($id);
            }
        }else{
            return null;
        }
    }
    
    public function num_rows(){
        return $this->result->num_rows();
    }
    
    public function getid(){
        return $this->{$this->primary_key};
    }
    
    public function result(){
        return $this->result->result();
    }
    /*** Funcion unica de guardado, Si el id esta registrado lo actualiza si no pues lo inserta. ***/
    /*** ForceInsert es para que ignore el id, lo inserte siempre ***/
    public function save($data = array(),$id = '',$forceInsert = FALSE){
        if(empty($data)){
            $field_data = $this->db->field_data($this->tablename);
            $data = array();
            foreach($field_data as $f){
                if($f->name!='id'){
                    $field = $f->name;
                    if(isset($this->$field) && !empty($this->$field)){
                        $data[$field] = $this->$field;
                    }
                }
            }
        }
        if(empty($id) && !empty($this->{$this->primary_key})){
            $id = $this->getid();
        }
        //Si se recibe un objeto se transforma en array
        if(is_object($data)){
            $data = (array)$data;
        }
        unset($data[$this->primary_key]); //Por ningun motivo se puede enviar el id a bd para que lo actualice
        if(!isset($id) || empty($id) || $forceInsert){
            $this->db->insert($this->tablename,$data);
            $this->id = $this->db->insert_id();
        }else{
            $this->db->update($this->tablename,$data,array($this->tablename.'.id'=>$id));
        }        
    }
    
    public function remove($id = ''){
        if(empty($id)){
            foreach($this->where as $n=>$j){             
                if($j==NULL || $j==null){
                    $this->db->where($n,NULL,FALSE);
                }else{
                    $this->db->where($n,$j);
                }
            }
            $this->db->delete($this->tablename);
        }else{
            $this->db->where($this->tablename.'.id',$id);
            $this->db->delete($this->tablename);
        }
    }
    
    //Encontrar un valor por su atributo sin consultar a la bd    
    public function search($id,$attr = 'id'){
        foreach($this->result() as $t){
            if($t->{$attr} == $id){
                return $t;
                die();
            }
        }
        return null;
    }
    
    public function getJSON($encode = true){
        $e = array();
        foreach($this->result() as $est){
            $e[] = $est;
        }
        return json_encode($e);
    }
}
?>
